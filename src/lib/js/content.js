export const artists = [
	{
		"name": "Rees",
		"role": "Founder, Artist",
		"description": "Rees is the founder and pioneer of our brand.",
		"image": "rees.jpg",
		"links": [
		  {
			"type": "instagram",
			"name": "artistic3vil",
			"link": "https://www.instagram.com/artistic3vil/"
		  },
		  {
			"type": "spotify",
			"name": "Rees",
			"link": "https://open.spotify.com/artist/4T13Lk20kY34QdsxnYWsqg"
		  },
		  {
			"type": "youtube",
			"name": "ArtisticEvi",
			"link": "https://www.youtube.com/channel/UCVHRF4WwBA5dZTN-vM097aA"
		  }
		]
	  },
	{
		"name": "JasonAtake",
		"role": "Artist",
		"description": "pünkangël",
		"image": "jason.jpg",
		"links": [
		  {
			"type": "instagram",
			"name": "jasonatake",
			"link": "https://www.instagram.com/jasonatake/"
		  },
		  {
			"type": "spotify",
			"name": "JasonAtake",
			"link": "https://open.spotify.com/artist/3I2gG6tQjibskETyrq7LNP"
		  },
		  {
			"type": "youtube",
			"name": "JasonAtake",
			"link": "https://youtube.com/@JasonAtake"
		  }
		]
	  }
];
